eclipse-platform-runtime (4.26-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patch
    - Build org.eclipse.core.jobs and org.eclipse.core.runtime with Java 11
    - Updated the bundles base directory
  * Track and download the new releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 03 Jan 2023 09:10:21 +0100

eclipse-platform-runtime (4.23-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patch
    - Build org.eclipse.e4.core.contexts with Java 11

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 08 Dec 2022 11:56:14 +0100

eclipse-platform-runtime (4.21-1) unstable; urgency=medium

  [ Emmanuel Bourg]
  * New upstream release
    - Require Java 11+
  * Standards-Version updated to 4.6.1

  [ Sudip Mukherjee ]
  * Fixed the javax.annotation version range in the OSGi metadata
    (Closes: #1020298)

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 15 Nov 2022 09:56:14 +0100

eclipse-platform-runtime (4.19-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.6.0.1
  * Track the new releases from git.eclipse.org (the GitHub repository is gone)

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 09 Oct 2021 16:47:19 +0200

eclipse-platform-runtime (4.18-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 26 Dec 2020 23:37:43 +0100

eclipse-platform-runtime (4.17-1) unstable; urgency=medium

  * New upstream release
    - Depend on libequinox-common-java (>= 3.13)
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 02 Oct 2020 08:57:24 +0200

eclipse-platform-runtime (4.16-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 28 Jun 2020 22:48:35 +0200

eclipse-platform-runtime (4.15-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.5.0
  * Switch to debhelper level 12

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 25 Apr 2020 16:32:11 +0200

eclipse-platform-runtime (4.13-1) unstable; urgency=medium

  * New upstream release
    - Updated the bundle dependencies
  * Standards-Version updated to 4.4.1

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 28 Dec 2019 20:15:34 +0100

eclipse-platform-runtime (4.12-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 17 Jul 2019 11:39:32 +0200

eclipse-platform-runtime (4.11-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Jul 2019 10:15:37 +0200

eclipse-platform-runtime (4.10-1) unstable; urgency=medium

  * New upstream release
  * The license changed to EPL-2.0

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 28 Dec 2018 09:34:05 +0100

eclipse-platform-runtime (4.9-1) unstable; urgency=medium

  * New upstream release
  * Depend on libeclipse-osgi-services-java (>= 3.7.0)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 03 Dec 2018 10:50:34 +0100

eclipse-platform-runtime (4.8-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 27 Nov 2018 12:29:00 +0100

eclipse-platform-runtime (4.7.3-2) unstable; urgency=medium

  * Enabled more bundles
  * Standards-Version updated to 4.2.1

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 05 Sep 2018 15:51:58 +0200

eclipse-platform-runtime (4.7.3-1) unstable; urgency=medium

  * Initial release (Closes: #903632)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 12 Jul 2018 11:23:26 +0200
